//
//  CustomUIButton.m
//  WhoIsMyRep
//
//  Created by Chris Dillard on 3/6/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CustomUIButton.h"


@implementation CustomUIButton

+ (id)buttonWithType:(UIButtonType)buttonType {
    CustomUIButton *toReturn = [super buttonWithType:buttonType];
    toReturn.imageView.contentMode = UIViewContentModeScaleAspectFit;
    return toReturn;
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect {
    return contentRect;
}
@end