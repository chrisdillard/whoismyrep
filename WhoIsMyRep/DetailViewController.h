//
//  DetailViewController.h
//  CodingAssignment
//
//  Created by Chris Dillard on 3/6/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController {
    dispatch_queue_t imageQueue;
}

//
// image view to be popuplated with full size image
//
@property (weak, nonatomic) IBOutlet UILabel *description;

//
// the backing dictionary for selected detail item
//
@property (weak, nonatomic)     NSDictionary* detailItem;


-(IBAction)call:(id)sender;


@end
