//
//  DetailViewController.m
//  CodingAssignment
//
//  Created by Chris Dillard on 3/6/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//

#import "DetailViewController.h"

#import "MapViewController.h"
#import "WebViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    imageQueue = dispatch_queue_create("Image Detail Queue",NULL);
        
    NSString *name = [self.detailItem objectForKey:@"name"];
    
    NSString *party = [self.detailItem objectForKey:@"party"];
    
    NSString *district = [self.detailItem objectForKey:@"district"];
    
    
    NSString* senOrRep=@"Sen.";
    //determine if this result is senator or rep by parsing district
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([district rangeOfCharacterFromSet:notDigits].location == NSNotFound) {
        senOrRep=@"Rep.";
    }
    
    NSString *composedTitle = [NSString stringWithFormat:@"%@ %@ (%@)",senOrRep,name,party];
    [self setTitle:composedTitle];
}


//action handlers
-(IBAction)call:(id)sender {
    NSString *phone = [self.detailItem objectForKey:@"phone"];

    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phone]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"map"]) {
        NSString *link = [self.detailItem objectForKey:@"office"];

        
        NSString *state = [self.detailItem objectForKey:@"state"];

        
        // Get reference to the destination view controller
        MapViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        [vc setAddress:[NSString stringWithFormat:@"%@, %@",link,state]];
    }
    else     if ([[segue identifier] isEqualToString:@"web"]) {
        // Get reference to the destination view controller
        WebViewController *vc = [segue destinationViewController];
        NSString *link = [self.detailItem objectForKey:@"link"];

        // Pass any objects to the view controller here, like...
        [vc setLink:link];
    }

    
}


@end
