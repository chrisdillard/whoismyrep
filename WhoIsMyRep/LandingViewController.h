//
//  LandingViewController.h
//  WhoIsMyRep
//
//  Created by Chris Dillard on 3/11/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LandingViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIImageView *tapForRepsImageView;
@property (weak, nonatomic) IBOutlet UILabel *tapForReps;

@end
