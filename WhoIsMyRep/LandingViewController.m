//
//  LandingViewController.m
//  WhoIsMyRep
//
//  Created by Chris Dillard on 3/11/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LandingViewController.h"
#import "ViewController.h"

@implementation LandingViewController : UIViewController


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Make sure your segue name in storyboard is the same as this line
        // Get reference to the destination view controller
        ViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        [vc setMode:[segue identifier]];
    
}

@end