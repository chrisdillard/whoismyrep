//
//  LandingViewController.h
//  WhoIsMyRep
//
//  Created by Chris Dillard on 3/11/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController


@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) NSString *address;


-(void) loadAddress:(NSString*) location ;
@end
