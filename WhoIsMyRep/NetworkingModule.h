//
//  NetworkingModule.h
//  CodingAssignment
//
//  Created by Chris Dillard on 3/6/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
@interface NetworkingModule : NSObject {
    
    //operation queue for network events
    NSOperationQueue* operationQueue;
}

//
// setup n/w module as singleton
//
+ (NetworkingModule*)sharedInstance;

//
// perform zip search with code
//
-(void) zipSearchWithZip :(NSString*)zip;

//
// perform senator state search with state code. (ie: CA, UT)
//
-(void) senatorStateSearchWithStateCode :(NSString*)stateCode;

//
// perform representative state search with state code. (ie: CA, UT)
//
-(void) representativeStateSearchWithStateCode :(NSString*)stateCode;

//
// generic http wrapper
//
-(void) runRequest:(NSURLRequest*) req ;

@end
