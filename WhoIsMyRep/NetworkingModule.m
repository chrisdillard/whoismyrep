
//
//  NetworkingModule.m
//  CodingAssignment
//
//  Created by Chris Dillard on 3/6/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NetworkingModule.h"

//
// static flickr api url to have keyword appended
//
#define ZIPSEARCHAPI @"http://whoismyrepresentative.com/getall_mems.php?output=json&zip="


#define SENSTATESEARCHAPI @"http://whoismyrepresentative.com/getall_sens_bystate.php?output=json&state="

#define REPSTATESEARCHAPI @"http://whoismyrepresentative.com/getall_reps_bystate.php?output=json&state="

#define RESULTS_UPDATED @"resultsUpdated"

@implementation NetworkingModule

//
// setup n/w module as singleton
//
+ (NetworkingModule *)sharedInstance {
    static dispatch_once_t once;
    static NetworkingModule *instance;
    dispatch_once(&once, ^{
        instance = [[NetworkingModule alloc] init];
    });
    return instance;
}

//
// this function performs the whoismyrep search api with zip
// the end result from a succesful api call is "resultsUpdated" will be broadcast to notification center
//
-(void) zipSearchWithZip :(NSString*)zip {
    
    //create nsurlrequest from static whoismyrep url and zip
    NSString* api = [NSString stringWithFormat:@"%@%@",ZIPSEARCHAPI,zip];
    NSURLRequest* req = [NSURLRequest requestWithURL:[NSURL URLWithString:api]];
    
    [self runRequest:req];
}


//
// this function performs the whoismyrep senator search api with zip
// the end result from a succesful api call is "resultsUpdated" will be broadcast to notification center
//
-(void) senatorStateSearchWithStateCode :(NSString*)stateCode {
    
    //create nsurlrequest from static whoismyrep url and zip
    NSString* api = [NSString stringWithFormat:@"%@%@",SENSTATESEARCHAPI,stateCode];
    NSURLRequest* req = [NSURLRequest requestWithURL:[NSURL URLWithString:api]];
    
    [self runRequest:req];
}

//
// this function performs the whoismyrep  representative search api with state code
// the end result from a succesful api call is "resultsUpdated" will be broadcast to notification center
//

-(void) representativeStateSearchWithStateCode :(NSString*)stateCode{
    //create nsurlrequest from static whoismyrep url and zip
    NSString* api = [NSString stringWithFormat:@"%@%@",REPSTATESEARCHAPI,stateCode];
    NSURLRequest* req = [NSURLRequest requestWithURL:[NSURL URLWithString:api]];
    
    [self runRequest:req];
}


//
// generic http handler
//
-(void) runRequest:(NSURLRequest*) req {
    //create operation queue if first api call
    if (operationQueue==nil)
        operationQueue= [[NSOperationQueue alloc] init];
    
    //run nsurlconnection on network.
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    
    [NSURLConnection sendAsynchronousRequest:req queue:operationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data!=nil){
            //parse returned data using nsjsonserialization
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if (json!=nil){
                //get results array
                NSArray* results = [json objectForKey:@"results"];
                //send result to observers
                [[NSNotificationCenter defaultCenter] postNotificationName:RESULTS_UPDATED object:results];
                
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                
            }
        }
    }
     ];
}



@end