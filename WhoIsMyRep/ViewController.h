//
//  ViewController.h
//  CodingAssignment
//
//  Created by Chris Dillard on 3/11/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate> {
    
    //
    // the foundation object from results JSON array
    //
    NSArray* results;
    
    //
    // external UIImage cache so that when scrolling in search results does not redownload.
    //
    NSMutableDictionary* photoCache;
    
    //
    // selected item to be passed to detail view controller.
    //
    NSDictionary* selectedItem;
    
    //
    // gcd queue for imageview thumbs in tableview cells.
    //
    dispatch_queue_t imageQueue;
}

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) NSString *mode;


@end

