//
//  ViewController.m
//  CodingAssignment
//
//  Created by Chris Dillard on 3/11/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//

#import "ViewController.h"
#import "NetworkingModule.h"
#import "DetailViewController.h"

//
// to be listened to for new photos from n/w module
//
#define RESULTS_UPDATED @"resultsUpdated"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //register for networking module updates
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resultsUpdated:)
                                                 name:RESULTS_UPDATED
                                               object:nil];
    // zero out data source
    results=[NSArray array];
    
    //create gcd queue for thumbs
    imageQueue = dispatch_queue_create("Image Queue",NULL);
    
    
    //depending upon mode customize search type
    if ([self.mode isEqualToString:@"zip"]) {
        [self.searchBar setPlaceholder:@"Enter ZIP Code"];
        [self setTitle:@"Search By ZIP"];
    }
    else if ([self.mode isEqualToString:@"sens"]) {
        [self.searchBar setPlaceholder:@"Get Senators by STATE (ie: CA,UT)"];
        [self setTitle:@"Search Senators By State"];
    }
    else if ([self.mode isEqualToString:@"reps"]) {
        [self.searchBar setPlaceholder:@"Get Representatives by STATE (ie: CA,UT)"];
        [self setTitle:@"Search Reps. By State"];

    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark handle storyboard segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"showDetailItem"])
    {
        // Get reference to the destination view controller
        DetailViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        [vc setDetailItem:selectedItem];
    }
}


#pragma mark tableview logic

//
// clear the tableview because new data or user cleared
//
-(void) clearAndReloadTableView {

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

#pragma mark photos updated

//
// this function is called when n/w module has new data from whoismyrep api call
//
- (void) resultsUpdated:(NSNotification *) notification {
    if (notification!=nil){
        NSArray* received = [notification object];
        if (received!=nil){
            
            //update table view data
            results=received;
            
            //clear photo cache
            photoCache=[NSMutableDictionary dictionary];

            //clear current data and reload table view using photos array
            [self clearAndReloadTableView];
        }
    }
}

#pragma mark UISearchBarDelegate

-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    //clear tableview when search bar zeroed out
    if ([[searchBar text] isEqualToString:@""]){
        results = [NSArray array];
        photoCache=[NSMutableDictionary dictionary];
        [self clearAndReloadTableView];
    }
    // if user has typed something then perform whoismyrep api call
    else{
        
        if ([self.mode isEqualToString:@"zip"]) {
            [[NetworkingModule sharedInstance] zipSearchWithZip:[searchBar text]];
        }
        else if ([self.mode isEqualToString:@"sens"]) {
            [[NetworkingModule sharedInstance] senatorStateSearchWithStateCode:[searchBar text]];
 
        }
        else if ([self.mode isEqualToString:@"reps"]) {
            [[NetworkingModule sharedInstance] representativeStateSearchWithStateCode:[searchBar text]];
        }
    }
    
    [searchBar setText:@""];
    [searchBar resignFirstResponder];
}


#pragma mark UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [results count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"result";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSString *name = [[results objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    NSString *party = [[results objectAtIndex:indexPath.row] objectForKey:@"party"];
    
    NSString *district = [[results objectAtIndex:indexPath.row] objectForKey:@"district"];

    
    NSString* senOrRep=@"Sen.";
    //determine if this result is senator or rep by parsing district
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([district rangeOfCharacterFromSet:notDigits].location == NSNotFound) {
        senOrRep=@"Rep.";
    }
    
    NSString *composedTitle = [NSString stringWithFormat:@"%@ %@ (%@)",senOrRep,name,party];
    
    cell.textLabel.text = composedTitle;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //setup our selected item for segue prepare
    selectedItem=[results objectAtIndex:indexPath.row];
    //perform segue to detail view controller with this object dict.
    [self performSegueWithIdentifier:@"showDetailItem" sender:self];
}

@end
