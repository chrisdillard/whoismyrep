//
//  LandingViewController.m
//  WhoIsMyRep
//
//  Created by Chris Dillard on 3/11/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "WebViewController.h"


@implementation WebViewController : UIViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self loadWebsite:self.link];
}

-(void) loadWebsite:(NSString*)site {
    
    [self setTitle:site];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:site]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end